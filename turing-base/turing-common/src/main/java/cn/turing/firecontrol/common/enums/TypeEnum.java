package cn.turing.firecontrol.common.enums;

/**
 * @author gx
 */
public enum TypeEnum {

    /**
     * 短信方式
     */
    SMS(1, "SMS"),

    /**
     * 电话语音方式
     */
    CALL(2, "Call"),

    /**
     * 短信和语音方式
     */
    ALL(3,"All"),

    /**
     * 邮件方式通知
     */
    Email(4,"Email"),

    /**
     * 广播形式通知
     */
    Broadcast(5,"Broadcast");

    /**
     * 类型
     */
    private int type;

    /**
     * 类型描述
     */
    private String warnType;


     TypeEnum(int type, String warnType){
        this.type = type;
        this.warnType = warnType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getWarnType() {
        return warnType;
    }

    public void setWarnType(String warnType) {
        this.warnType = warnType;
    }

    public static String getWarnByType(int type){

        for(TypeEnum en : values()){

            if (en.getType() == type){
                return en.getWarnType();
            }
        }
        return null;
    }
}
